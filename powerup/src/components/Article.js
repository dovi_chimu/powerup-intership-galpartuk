import React from "react";

const Article = (props) => {
    console.log(props.article)
    return(
        <div className="ui segment">
            <div className="content">
                <div className="header">
                    <img alt="avatar" className="ui mini circular image" src={props.article.source_image} />
                       {props.article.publisher}
                </div>
                <div className="meta">{props.article.created}</div>
                <div className="description">
                    {props.article.description}
                </div>
                <img alt= "" src={props.article.image} width='200px'/>
                <div>
                    <a href={props.article.url} target="_blank" rel="noopener noreferrer">read more...</a>
                                        
                <i className="thumbs up icon" onClick={(e) => e.target.style.color = 'red'}  />
                </div>
            </div>
        </div>
        );

};

export default Article
