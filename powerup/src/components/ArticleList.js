import React from "react";
import Article from "./Article";


const ArticleList = (props) =>
{
    const generatedArticles = props.articles.map((article) => {
        return <Article key={article.id} article={article} />
    })
    return (
        <div className={"ui segment container"}>
            {generatedArticles}
        </div>
    )

}

export default ArticleList
