import axios from "axios";

export default axios.create({
    baseURL: 'https://api.dev.chimu.io/scraper/interns/articles/'
})